var openalpr = require ("./openalpr.js");
const http = require('https');
const fs = require('fs');
var tempPlateLocation = `${__dirname}/licensePlate-US.jpg`
var remoteLicensePlate = "https://cdn.shinobi.video/images/test/licensePlate-US.jpg"

openalpr.Start(`${__dirname}/openalpr.conf`, null, null, true, 'us');

openalpr.GetVersion()

function identifyPlate (id, path,callback) {
    console.log(path)
    openalpr.IdentifyLicense (fs.readFileSync(path), {}, function (error, output) {
        var results = output.results
        if(callback)callback(error,results)
    })
}

// Basic starting
var getTestFile = function(callback){
    if(!fs.existsSync(tempPlateLocation)){
        const file = fs.createWriteStream(tempPlateLocation);
        const request = http.get(remoteLicensePlate, function(response) {
            response.pipe(file)
            file.on('close',function(){
                if(callback)callback(tempPlateLocation)
            })
        })
    }else{
        if(callback)callback(tempPlateLocation)
    }
}
var removeTestFile = function(callback){
    fs.unlink(tempPlateLocation,callback || function(){})
}


getTestFile(function(){
    for (var i = 0; i < 2; i++) {
        identifyPlate(i, tempPlateLocation, function(err,results){
            if(err)console.log(err)
            console.log(i, results)
            if(i === 2){
                removeTestFile()
            }
        })
    }
})
